public class Student implements Cloneable {

    private int age;

    private String name;


    public Student(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Student student = new Student(100, "小明");
        Student studentClone = (Student) student.clone();
        student.name = "八戒";
        System.out.println(student.name == studentClone.name);
        System.out.println(student.name);
        System.out.println(studentClone.name);


        a:
        for (int i = 0; i < 10; i++) {
            b:
            for (int j = 0; j < 10; j++) {
                System.out.println(j);
                break;
            }
        }
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        Student student = (Student) super.clone();
        student.name = this.name;
        return super.clone();
    }
}
