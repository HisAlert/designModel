package com.study.aspacts;


import com.study.annotations.LoginAn;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LoginAspect {

    /**
     * 命名切点
     * public 切点可访问性修饰符
     *         与类可访问性修饰符的功能是相同的，它可以决定定义的切点可以在哪些类中可使用。
     * pointCut 切点名称
     * void   返回类型
     *
     *     因为命名切点仅利用方法名及访问修饰符的信息，
     *  一般定义方法的返回类型为 void ，并且方法体为空
     */
    @Pointcut(value = "@annotation(com.study.annotations.LoginAn)")
    public void pointCut(){}


    @Pointcut("execution(public String com.study.controller.AspectController.*(..))")
    public void BrokerAspect(){

    }

    /**
     * 环绕通知：灵活自由的在目标方法中切入代码
     */
    @Around("pointCut()")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 获取目标方法的名称
        String methodName = joinPoint.getSignature().getName();
        // 获取方法传入参数
        Object[] params = joinPoint.getArgs();
        System.out.println("==@Around== lingyejun blog logger --》 method name " + methodName + " args " + params[0]);
    }

    @AfterReturning("pointCut()")
    public void sysNotic(JoinPoint joinPoint){
        log.info("呜呜呜呜呜呜呜"+joinPoint.toString());
        System.out.println("后");
    }

}
