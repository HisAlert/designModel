package com.study.controller;


import com.alibaba.fastjson.JSONObject;
import com.study.annotations.LoginAn;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AspectController {


    @RequestMapping("/workQueueCon/{string}")
    @LoginAn
    public JSONObject workQueueCon(@PathVariable String string) {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("1","2");
//        rabbitTemplate.convertAndSend(RabbitmqConfig.EXCHANGE_TOPICS_INFORM, string, jsonObject);
        return jsonObject;
    }

    @GetMapping(value = "hello")
    @LoginAn
    public String hello(String name) {
        return "Hello " + name;
    }


}
