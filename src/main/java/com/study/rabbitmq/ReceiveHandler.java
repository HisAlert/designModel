package com.study.rabbitmq;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class ReceiveHandler {

    //监听sms队列
    @RabbitListener(queues = {"user.order.receive_queue"})
    public void receive(String msg, Channel channel) {
        System.out.println("收到延时消息了:" + System.currentTimeMillis());
    }

    //监听sms队列
    @RabbitListener(queues = {"user.order.delay_queue"})
    public void receives(String msg, Channel channel) {
        System.out.println("收到延时消息了:" + System.currentTimeMillis());
    }

}