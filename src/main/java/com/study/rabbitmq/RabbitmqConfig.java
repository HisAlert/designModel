package com.study.rabbitmq;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerContainerFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RabbitmqConfig {

    public static final String QUEUE_INFORM_EMAIL = "queue_inform_email";
    public static final String QUEUE_INFORM_SMS = "queue_inform_sms";
    public static final String EXCHANGE_TOPICS_INFORM = "exchange_topics_inform";

    public static final String EXCHANGE_DELAY = "exchange_topics_delay";



    /**
     * 死信交换机
     *
     * @return
     */
    @Bean
    public DirectExchange userOrderDelayExchange() {
        return new DirectExchange("user.order.delay_exchange");
    }

    /**
     * 死信队列
     *
     * @return
     */
    @Bean
    public Queue userOrderDelayQueue() {
        Map<String, Object> map = new HashMap<>(16);
        map.put("x-dead-letter-exchange", "user.order.receive_exchange");
        map.put("x-dead-letter-routing-key", "user.order.receive_key");
        return new Queue("user.order.delay_queue", true, false, false, map);
    }

    /**
     * 给死信队列绑定交换机
     *
     * @return
     */
    @Bean
    public Binding userOrderDelayBinding() {
        return BindingBuilder.bind(userOrderDelayQueue()).to(userOrderDelayExchange()).with("user.order.delay_key");
    }


    /**
     * 死信接收交换机
     *
     * @return
     */
    @Bean
    public DirectExchange userOrderReceiveExchange() {
        return new DirectExchange("user.order.receive_exchange");
    }

    /**
     * 死信接收队列
     *
     * @return
     */
    @Bean
    public Queue userOrderReceiveQueue() {
        return new Queue("user.order.receive_queue");
    }

    /**
     * 死信交换机绑定消费队列
     *
     * @return
     */
    @Bean
    public Binding userOrderReceiveBinding() {
        return BindingBuilder.bind(userOrderReceiveQueue()).to(userOrderReceiveExchange()).with("user.order.receive_key");
    }


//(((((((((((((((((((((((((((((((((((((((((((((((((((

    @Bean(EXCHANGE_TOPICS_INFORM)
    public Exchange EXCHANGE_TOPICS_INFORM() {
        //durable(true)持久化，消息队列重启后交换机仍然存在
        return ExchangeBuilder.topicExchange(EXCHANGE_TOPICS_INFORM).durable(true).build();
    }


//    /**
//     *      * 延时队列交换机
//     *      * 注意这里的交换机类型：CustomExchange
//     *      *
//     *      * @return
//     */
//    @Bean("delay_exchange")
//    public CustomExchange delayExchange() {
//        Map<String, Object> args = new HashMap<>();
//        args.put("x-delayed-type", "direct");
//        //属性参数 交换机名称 交换机类型 是否持久化 是否自动删除 配置参数
//        return new CustomExchange("delay_exchange", "queueWorkOrder", true, false, args);
//    }


    //声明队列
    @Bean(QUEUE_INFORM_SMS)
    public Queue QUEUE_INFORM_SMS() {
        Queue queue = new Queue(QUEUE_INFORM_SMS);
        return queue;
    }

    //声明队列
    @Bean("queueWorkOrder")
    public Queue queueWorkOrder() {
        Queue queue = new Queue("work-order");
        return queue;
    }


    //声明队列
    @Bean(QUEUE_INFORM_EMAIL)
    public Queue QUEUE_INFORM_EMAIL() {
        Queue queue = new Queue(QUEUE_INFORM_EMAIL);
        return queue;
    }

    //channel.queueBind(INFORM_QUEUE_SMS,"inform_exchange_topic","inform.#.sms.#"); * 绑定队列到交换机
    @Bean
    public Binding BINDING_QUEUE_INFORM_SMS(@Qualifier(QUEUE_INFORM_SMS) Queue queue,
                                            @Qualifier(EXCHANGE_TOPICS_INFORM) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("inform.#.sms.#").noargs();
    }

    @Bean
    public Binding BINDING_QUEUE_INFORM_EMAIL(@Qualifier(QUEUE_INFORM_EMAIL) Queue queue,
                                              @Qualifier(EXCHANGE_TOPICS_INFORM) Exchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("inform.#.email.#").noargs();
    }

//    @Bean
//    public Binding bindingBuilderDelay(@Qualifier("queueWorkOrder") Queue queue,
//                                       @Qualifier("delay_exchange") Exchange exchange) {
//        return BindingBuilder.bind(queue).to(exchange).with("delay-key").noargs();
//    }

//    @Bean
//    public Binding WorkQueueExchange(@Qualifier("queueWorkOrder") Queue queue,
//                                     @Qualifier(EXCHANGE_TOPICS_INFORM) Exchange exchange) {
//        return BindingBuilder.bind(queue).to(exchange).with("inform.#").noargs();
//    }


    //    用代码设置手动确认
//    @Bean
//    public RabbitListenerContainerFactory<?> rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
//        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
//        factory.setConnectionFactory(connectionFactory);
//        factory.setMessageConverter(new Jackson2JsonMessageConverter());
//        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
//        return factory;
//    }

//===================================延迟队列============================================
    /**
     * 延迟队列TTL名称
     */
//    private static final String ORDER_DELAY_QUEUE="user.order.delay.queue";
//
//    public static final String ORDER_DALAY_EXCHANGE="user.order.delay.exchange";
//    /**
//     * routing key 名称
//     * 具体消息发送在该routingKey的
//     */
//    public static final String ORDER_DELAY_ROUTING_KEY="order_delay";
//
//    public static final String ORDER_QUEUE_NAME="user.order.queue";
//
//    public static final String ORDER_EXCHANGE_NAME="user.order.exchange";
//
//    private static final String ORDER_ROUTING_KEY="order";
//
//    @Bean
//    public Queue delayOrderQueue(){
//        Map<String,Object> map=new HashMap<>();
//        //x-dead-letter-exchange 声明队列里的死信准发的DLX名称
//        map.put("x-dead-letter-exchange",ORDER_EXCHANGE_NAME);
//        //x-dead-letter-routing-key 申明了这些死信在转发时携带的routing-key 名称
//        map.put("x-dead-letter-routing-key",ORDER_ROUTING_KEY);
//        return new Queue(EXCHANGE_DELAY,true,false,false,map);
//    }
//
//    /**
//     * 需要将一个队列绑定到一个交换机上(exchanage),要求该信息与一个特定的路由健完全匹配
//     * 这是一个完整的匹配。如果一个队列绑定到该交换机上要求路由健“dog”，则只有被标记为“dog”
//     * 不会转发dog。happy，也不会转发dog。guard 只会转发“dog”
//     *
//     */
//    @Bean
//    public DirectExchange orderDelayExchange(){
//        return new DirectExchange(ORDER_DELAY_QUEUE);
//    }
//
//    @Bean
//    public Binding dlxBinding(){
//        return BindingBuilder.bind(delayOrderQueue()).to(orderDelayExchange()).with(ORDER_DELAY_ROUTING_KEY);
//    }
//
//    @Bean
//    public Queue orderQueue(){
//         return new Queue(ORDER_QUEUE_NAME,true);
//    }
//
//    @Bean
//    public TopicExchange orderTopicExchange(){
//        return new TopicExchange(ORDER_EXCHANGE_NAME);
//    }
//
//    @Bean
//    public Binding orderBinding(){
//        return BindingBuilder.bind(orderQueue()).to(orderTopicExchange()).with(ORDER_ROUTING_KEY);
//    }

}
