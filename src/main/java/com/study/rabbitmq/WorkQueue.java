package com.study.rabbitmq;


import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class WorkQueue {

    @SneakyThrows
    @RabbitListener(queues = {"work-order"})
    private void process(String msg, Channel channel, Message message) {
//        TimeUnit.SECONDS.sleep(1);
        channel.basicQos(1);
        System.out.println("work-order: " + msg);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }


    @SneakyThrows
    @RabbitListener(queues = {RabbitmqConfig.QUEUE_INFORM_SMS})
    private void receive_smss(String msg, Channel channel, Message message) {
        channel.basicQos(1);
        System.out.println("queue_inform_sms: " + msg);
    }

    //监听email队列
    @SneakyThrows
    @RabbitListener(queues = {RabbitmqConfig.QUEUE_INFORM_EMAIL})
    public void receive_email(String msg, Channel channel, Message message) {
        channel.basicQos(1);
        System.out.println("queue_inform_email: " + msg);
    }


//    @SneakyThrows
//    @RabbitListener(queues = {RabbitmqConfig.ORDER_QUEUE_NAME})
//    public void receive_dalay(String msg, Channel channel, Message message){
//        channel.basicQos(1);
//        System.out.println("work-order: " + msg);
//        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
//    }

}
