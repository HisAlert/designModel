package com.study.service;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


@Qualifier("cake")
@Component
public class Cake extends Dessert {

    @Override
    public void eat() {
        System.out.println("吃蛋糕");
    }
}
