package com.study.service;


import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
public class IceCream extends Dessert {

    @Override
    public void eat() {
        System.out.println("冰淇凌");
    }

}
