package com.study.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;


/**
 * NO:2
 *
 */

@Component
public class BeanFactoryAwareImpl implements BeanFactoryAware {

    Logger logger = LoggerFactory.getLogger("BeanFactoryAware:生命周期测试");

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        logger.error("BeanFactoryAware:" + beanFactory.toString());
    }
}
