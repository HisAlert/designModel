package com.study.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextAwareImpl implements ApplicationContextAware {

    Logger logger= LoggerFactory.getLogger("ApplicationContextAware:生命周期测试");

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        logger.error("ApplicationContextAware=获取definition里面的参数个数:"+String.valueOf(applicationContext.getBeanDefinitionNames().length));
    }
}
