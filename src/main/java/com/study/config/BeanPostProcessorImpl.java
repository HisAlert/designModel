package com.study.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.util.Map;

//@Component
public class BeanPostProcessorImpl implements BeanPostProcessor {

    Logger logger= LoggerFactory.getLogger("BeanPostProcessor:生命周期测试");

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        logger.error("BeanPostProcessor:postProcessBeforeInitialization="+bean);
        logger.error("BeanPostProcessor:postProcessBeforeInitialization="+beanName);
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        logger.error("BeanPostProcessor:postProcessAfterInitialization="+bean);
        logger.error("BeanPostProcessor:postProcessAfterInitialization="+beanName);
        return null;
    }
}
