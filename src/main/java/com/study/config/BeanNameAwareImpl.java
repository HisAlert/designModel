package com.study.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.annotation.Configuration;

/**
 * NO:1
 * 实现BeanNameAware接口需要实现setBeanName()方法，这个方法只是简单的返回我们当前的beanName
 */

@Configuration
public class BeanNameAwareImpl implements BeanNameAware {

    Logger logger=LoggerFactory.getLogger("BeanNameAware:生命周期测试");

    @Override
    public void setBeanName(String s) {
        logger.error("BeanName:"+s);
    }

}
