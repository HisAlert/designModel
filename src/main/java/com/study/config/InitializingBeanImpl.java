package com.study.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * Spring为bean提供了两种初始化bean的方式，实现InitializingBean接口，
 * 实现afterPropertiesSet方法，或者在配置文件中通过init-method指定，两种方式可以同时使用
 */
@Component
public class InitializingBeanImpl implements InitializingBean, BeanPostProcessor {

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("赵莹莹石呆子");
    }
}
