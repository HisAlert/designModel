package com.study;

import com.study.rabbitmq.RabbitmqConfig;
import com.study.service.Dessert;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.UUID;


@Configuration
@RestController
@ComponentScan("com.study.*")
@EnableAspectJAutoProxy(proxyTargetClass = true)
@SpringBootApplication
public class PerentApplication implements ApplicationContextAware {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    RabbitTemplate rabbitTemplate;

    public static void main(String[] args) {
        SpringApplication.run(PerentApplication.class, args);
    }

    @Autowired
    private Student student;


    @Qualifier("cake")
    @Autowired
    private Dessert dessert;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println(student.name);
        System.out.print("============");
        dessert.eat();
    }


    @Bean
    public Student getStudent() {
        return new Student("测试");
    }

    @RequestMapping("/setAmqp")
    private String setAmqp() {
        rabbitTemplate.convertAndSend(RabbitmqConfig.EXCHANGE_TOPICS_INFORM, "inform.1.sms.1", 123);
        return "SUCCESS";
    }


    @RequestMapping("/topicDelay")
    private String getAmqp() {
//        send("测试",5000);
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());
//        声明消息处理器  这个对消息进行处理  可以设置一些参数   对消息进行一些定制化处理   我们这里  来设置消息的编码  以及消息的过期时间  因为在.net 以及其他版本过期时间不一致   这里的时间毫秒值 为字符串
        MessagePostProcessor messagePostProcessor = message -> {
            MessageProperties messageProperties = message.getMessageProperties();
//            设置编码
            messageProperties.setContentEncoding("utf-8");
//            设置过期时间10*1000毫秒
            messageProperties.setExpiration("10000");
            return message;
        };
        rabbitTemplate.convertAndSend("user.order.delay_exchange", "user.order.delay_key", new Date().getSeconds(), messagePostProcessor, correlationData);
        return "SUCCESS";
    }

//    public void send(String msg, int delayTime) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        this.rabbitTemplate.convertAndSend(RabbitmqConfig.ORDER_DALAY_EXCHANGE, RabbitmqConfig.ORDER_DELAY_ROUTING_KEY, msg, message -> {
//            message.getMessageProperties().setExpiration(delayTime + ""); System.out.println(sdf.format(new Date()) + " Delay sent."); return message;
//        });
//    }


    @RequestMapping("/workQueue/{string}")
    private String workQueue(@PathVariable(required = false) String string) {
        while (true) {
            rabbitTemplate.convertAndSend(string, string);
        }
    }


    class Student {

        private String name;

        public Student(String name) {
            this.name = name;
        }
    }


}
