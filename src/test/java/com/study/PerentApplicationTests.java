package com.study;

import com.study.rabbitmq.RabbitmqConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PerentApplicationTests {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 储物柜 对应随机码生成策略判断是否出现重复
     */
    @Test
    public void testRedis() {
        if (redisTemplate.opsForHash().putIfAbsent("delivery_lock_order_random_number", "1", 1)) {
            System.out.println("成功返回:" + redisTemplate.opsForHash().get("delivery_lock_order_random_number", "1"));
        }
    }

    /**
     * rabbit 对应Exchange 添加消息
     */
    @Test
    public void testSendByTopics() {
        for (int i = 0; i < 5; i++) {
            String message = "sms email inform to user" + i;
            rabbitTemplate.convertAndSend(RabbitmqConfig.EXCHANGE_TOPICS_INFORM, "inform.sms.email", message);
        }
    }

    public static ReentrantLock lock = new ReentrantLock();
    /**
     * java 集合框架--保存 等待的线程
     */
    public static BlockingQueue<Thread> waiters = new LinkedBlockingQueue<>();


    public static final long count = 100000000l;

    public static void main(String[] args) throws InterruptedException {

//        concurrency();
        Integer[] a = {4, 3};

        for (int i = 1; i < a.length; i++) {
            for (int j = 0; j < a.length - i; j++) {
                if (a[j] > a[j + 1]) {
                    Integer temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }


        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }


    }

    public static void concurrency() throws InterruptedException {
        long start = System.currentTimeMillis();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int a = 0;
                for (int i = 0; i < count; i++) {
                    a += 5;
                }
            }
        });

        thread.start();

        int b = 0;

        for (int i = 0; i < count; i++) {
            b--;
        }

        long time = System.currentTimeMillis() - start;

        thread.join();
        System.out.println(time);
    }


}
