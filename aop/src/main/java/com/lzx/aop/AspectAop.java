package com.lzx.aop;


import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AspectAop {

    @Pointcut("@annotation(AroundAop)")
    public void pointCut() {
    }

}
