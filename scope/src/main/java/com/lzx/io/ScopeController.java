package com.lzx.io;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ScopeController {


    private LinstenMusic linstenMusic;


    @GetMapping("/scope")
    public String testScope() {
        linstenMusic.listen();
        return "SUCCESS";
    }

//    /**
//     * 会话级别的作用域
//     *
//     * @return String
//     */
//    @GetMapping("/scope-session")
//    public String testScopeSession() {
//        System.out.println(shoppingCart.getDate().getTime());
//        return "SUCCESS";
//    }
//
//    @Autowired
//    public void setLinstenMusic(LinstenMusic linstenMusic) {
//        this.linstenMusic = linstenMusic;
//    }
//
//    @Autowired
//    public void setStoreService(StoreService storeService) {
//        this.storeService = storeService;
//    }


}
