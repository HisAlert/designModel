package com.lzx.io;

import lombok.Data;
import org.omg.CORBA.Environment;
import org.springframework.beans.factory.annotation.Value;

@Data
public class HelloMessageGenerator {

    private String message;

    private Environment environment;

    @Override
    public String toString() {
        return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode());
    }
}