package com.lzx.io;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Scope2Controller {

    private LinstenMusic linstenMusic;

    @GetMapping("/scope2")
    public String testScope() {
        linstenMusic.listen();
        return "SUCCESS";
    }

    @Autowired
    public void setLinstenMusic(LinstenMusic linstenMusic) {
        this.linstenMusic = linstenMusic;
    }
}
