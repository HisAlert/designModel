package com.lzx.io;


import lombok.Data;

import java.util.Date;

@Data
public class ShoppingCart {

    private Date date;

    public ShoppingCart(Date date) {
        this.date = date;
    }
}
