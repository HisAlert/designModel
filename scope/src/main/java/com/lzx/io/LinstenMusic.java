package com.lzx.io;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LinstenMusic {

    private final Date date;

    public void listen(){
        System.out.println
                ("我在"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.date)+":听了一场演唱会!!!");
    }

    public LinstenMusic() {
        this.date=new Date();
    }

}
