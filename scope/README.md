
**Profile**
-- 
- 配置文件中配置之后 不同部署环境使用
- 比如：不同数据源，不同redis连接

**Conditional**
--
- bean的先后顺序可控

**自动装配的歧义性**
--
- bean的解决歧义性3种方式
-  @Resourse j2ee
-  Autowird Qualifier(修饰语)
-  Qualifier 注解使用

**Bean的作用域**
--
 单例  Singleton
 原型  Prototype
 会话  Session
 请求  Request
****
--
 
 