## Bean 的生命周期


- Spring对Bean 先进行实例化
```java
new Bean()
```
- Spring将值和Bean的引用注入到Bean对应的属性中
```java
setBeanParameter
```
- 如果实现了Aware中的接口如:
```java
BeanNameAware BeanFactoryAware AplicationContextAware

BeanNameAware查看Name的值 是BeanMap中的Key

BeanFactoryAware 实现方法SetBeanFactory 将BeanFactory容器实例传入

AplicationContextAware 实现方法 setApplicationContext 获取到bean应用中的上下文

```
- 实现了BeanPostPrecessor 是 初始化接口实现的aop实现
InitializingBean afterPropertiesSet 方法

- 一方引用全部使用
ProcessBeforeInitizlization 前置方法
ProcessAfterInitialization 后置方法