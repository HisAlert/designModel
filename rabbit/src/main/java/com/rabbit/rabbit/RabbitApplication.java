package com.rabbit.rabbit;

import com.rabbit.rabbit.config.QueueEnum;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@SpringBootApplication
public class RabbitApplication implements ApplicationContextAware {

    public static void main(String[] args) {
        SpringApplication.run(RabbitApplication.class, args);
    }


    /**
     * RabbitMQ 模版消息实现类
     */
    @Autowired
    private AmqpTemplate rabbitMqTemplate;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        // 测试延迟10秒
//        messageProvider.sendMessage("测试延迟消费,写入时间：" + new Date(),
//                QueueEnum.MESSAGE_TTL_QUEUE.getExchange(),
//                QueueEnum.MESSAGE_TTL_QUEUE.getRouteKey(),
//                10000);

        rabbitMqTemplate.convertAndSend(QueueEnum.MESSAGE_TTL_QUEUE.getExchange(), QueueEnum.MESSAGE_TTL_QUEUE.getRouteKey(), "测试延迟消费,写入时间：" + new Date(), message -> {
            // 设置延迟毫秒值
            message.getMessageProperties().setExpiration(String.valueOf(10000));
            return message;
        });
    }

    @RequestMapping("/get")
    public String send() {
        rabbitMqTemplate.convertAndSend(QueueEnum.MESSAGE_TTL_QUEUE.getExchange(), QueueEnum.MESSAGE_TTL_QUEUE.getRouteKey(), "测试延迟消费,写入时间：" + new Date(), message -> {
            // 设置延迟毫秒值
            message.getMessageProperties().setExpiration(String.valueOf(10000));
            return message;
        });
        return "success";
    }


}
